<?php

namespace SemanticLab\Validator\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class ValidatorMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validator:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create request validators';

    private $files = '';

    private $jsonConfigDirPath = '';

    private $requestDirPath = '';

    private $requestNamespace = 'App\Http\Requests';

    private $responseDirPath = '';

    private $responseNamespace = '';

    private $failResponseConfig = null;

    private $stubPath = '';

    private $whitespace = "\r\n        ";

    private $rules = '';

    private $errorMessages = '';

    /**
     * Create a new command instance.
     *
     * @param  \Illuminate\Filesystem\Filesystem $files
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
        $this->stubPath = __DIR__.'/../stubs/';
        $this->jsonConfigDirPath = config_path('validators');
        $this->requestDirPath = app_path('Http/Requests');
        $this->responseDirPath = app_path('Http/Responses');
        $this->makeDirs($this->jsonConfigDirPath);
        $this->makeDirs($this->requestDirPath);
        $this->makeDirs($this->responseDirPath);
        $this->createValidatorResponse();

    }

    /**
     * Execute the console command.
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $this->responseNamespace = config('validator.response_namespace');
        $files = $this->files->allFiles($this->jsonConfigDirPath);
        foreach($files as $file) {
            if ($file->getExtension() === "json") {
                $this->generateValidatorsByFile($file->getPathname());
            }
        }
    }

    /**
     * Generate Validator by JSON config file
     *
     * @param $jsonFilePath
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function generateValidatorsByFile($jsonFilePath) {
        $json = json_decode($this->files->get($jsonFilePath));

        // initial default failResponse
        if (!empty($json->failResponse)) {
            $this->failResponseConfig = $this->initFailResponse($json->failResponse);
        } else {
            $this->failResponseConfig = new \stdClass();
            $this->failResponseConfig->type = "default";
        }

        // initial request namespace
        $subNamespace = null;
        if (!empty($json->subNamespace)) {
            $subNamespace = $json->subNamespace;
        }

        // generate all validators
        $validators = $json->validators;
        foreach ($validators as $validatorName => $validator) {
            $this->rules = "[{$this->whitespace}";
            $this->errorMessages = "[{$this->whitespace}";
            $this->createRequestValidator($subNamespace, $validatorName, $validator);
        }
    }

    /**
     * Generate dir(s) in disk with given path
     *
     * @param $path
     */
    private function makeDirs($path) {
        if (!is_dir($path)) {
            dump($path);
            mkdir($path, 0777, true);
        }
    }

    /**
     * Create ValidatorResponse at response dir path in config
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function createValidatorResponse() {
        // interface
        $iValidatorResponseInterface = "{$this->responseDirPath}/IValidatorResponse.php";
        $iValidatorResponseStub = $this->files->get("{$this->stubPath}IValidatorResponse.stub");
        $this->files->put($iValidatorResponseInterface, $iValidatorResponseStub);
        // class
        $validatorResponseClass = "{$this->responseDirPath}/ValidatorResponse.php";
        $validatorResponseStub = $this->files->get("{$this->stubPath}ValidatorResponse.stub");
        $this->files->put($validatorResponseClass, $validatorResponseStub);
    }

    /**
     * Initialize fail response from JSON config
     *
     * @param $failResponse
     * @return object
     */
    private function initFailResponse($failResponse) {
        $result = [];
        if (!empty($failResponse->class)) {
            $result["type"] = "response";
            $result["class"] = $failResponse->class;
        } else if (!empty($failResponse->type)) {
            if ($failResponse->type === "response" && empty($failResponse->class)) {
                $result["class"] = "ValidatorResponse";
            }
            $result["type"] = $failResponse->type;
        } else {
            $result["type"] = "default";
        }

        if (!empty($failResponse->httpStatus)) {
            $result["httpStatus"] = $failResponse->httpStatus;
        } else {
            $result["httpStatus"] = 422;
        }

        return (object) $result;
    }

    /**
     * Response path regex for let all paths end width '\'
     *
     * @param $path
     * @return string
     */
    private function pathRegex($path) {
        return (preg_match('/.*\\$/', $path)) ? $path : "{$path}\\";
    }

    /**
     * Parsing the fail response class information from JSON config
     *  for generate full path for import and response class name
     *
     * @param $failResponseClass
     * @return object
     */
    private function responseClassParse($failResponseClass) {
        $namespace = $this->pathRegex($this->responseNamespace);
        if (is_string($failResponseClass)) {
            $fullPath = "use {$namespace}{$failResponseClass};";
            $className = $failResponseClass;
        } else if (is_object($failResponseClass)) {
            $responseClassPath = $this->pathRegex($failResponseClass->namespace);
            $fullPath = "use {$namespace}{$responseClassPath}{$failResponseClass->name};";
            $className = $failResponseClass->name;
        } else {
            $fullPath = "use {$namespace}ValidatorResponse;";
            $className = "ValidatorResponse";
        }

        return (object) [
            "fullPath" => str_replace("\\\\", "\\", $fullPath),
            "className" => $className,
        ];
    }

    /**
     * Main function for create several FormRequest with validation rules
     *
     * @param string|null $subNamespace
     * @param string $validatorName
     * @param \stdClass $validator
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function createRequestValidator ($subNamespace, $validatorName, $validator)
    {
        $body = $validator->body;
        foreach($body as $prop => $val) {
            $this->propertyParser('', $prop, $val);
        }
        $this->rules .= ']';
        $this->errorMessages .= ']';
        if (!empty($validator->failResponse)) {
            $failResponse = $this->initFailResponse($validator->failResponse);
        } else {
            $failResponse = $this->failResponseConfig;
        }
        $this->buildClass($subNamespace, $validatorName, $this->rules, $this->errorMessages, $failResponse);
    }

    /**
     * Recursive function to parsing JSON data
     *
     * @param $fullKeyName
     * @param $key
     * @param $val
     */
    private function propertyParser($fullKeyName, $key, $val)
    {
        $currentName = ($fullKeyName === '') ? $key : "{$fullKeyName}.{$key}";
        if (is_string($val)) {
            $this->rules .= "    \"{$currentName}\" => \"{$val}\",{$this->whitespace}";
        } else if (is_object($val)) {
            foreach ($val as $propKey => $propVal) {
                if ($propKey === 'rules' && is_object($propVal)) {
                    $rules = "";
                    $first = true;
                    foreach ($propVal as $rule => $message) {
                        // rules
                        if ($first) {
                            $rules = $rule;
                            $first = false;
                        } else {
                            $rules .= "|{$rule}";
                        }
                        // message
                        if (!empty($message)) {
                            $rule = explode(":", $rule)[0];
                            $this->errorMessages .= "    \"{$currentName}.{$rule}\" => \"{$message}\",{$this->whitespace}";
                        }
                    }
                    $this->rules .= "    \"{$currentName}\" => \"{$rules}\",{$this->whitespace}";
                } else {
                    $this->propertyParser($currentName, $propKey, $propVal);
                }
            }
        } else if (is_array($val)) {
            $this->propertyParser($currentName, '*', $val[0]);
        }
    }

    /**
     * Function to generate the php class (FormRequest)
     *  default in app/Http/Requests/
     *
     * @param string|null $subNamespace
     * @param $className
     * @param $rules
     * @param $errorMessage
     * @param $failResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function buildClass($subNamespace, $className, $rules, $errorMessage, $failResponse)
    {
        // failResponse stub process:
        switch ($failResponse->type) {
            default:
            case "default":
                $failResponseStub = "";
                $failResponseClassPath = "";
                break;
            case "exception":
                $failResponseClassPath = "";
                $failResponseStub = $this->files->get("{$this->stubPath}ExceptionFailedValidation.stub");
                $httpStatus = ($failResponse->httpStatus) ? $failResponse->httpStatus : 422;
                $failResponseStub = str_replace(
                    ['dummyHttpStatus'],
                    [$httpStatus],
                    $failResponseStub
                );
                break;
            case "ignore":
                $failResponseClassPath = "";
                $failResponseStub = $this->files->get("{$this->stubPath}IgnoreFailedValidation.stub");
                break;
            case "response":
                $failResponseClass = $this->responseClassParse($failResponse->class);
                $failResponseClassPath = $failResponseClass->fullPath;
                $failResponseStub = $this->files->get("{$this->stubPath}ResponseFailedValidation.stub");
                $httpStatus = ($failResponse->httpStatus) ? $failResponse->httpStatus : 422;
                $failResponseStub = str_replace(
                    ['DummyResponse', 'dummyHttpStatus'],
                    [$failResponseClass->className, $httpStatus],
                    $failResponseStub
                );
                break;
        }

        // initial namespace
        $namespace = $this->requestNamespace;
        if (!empty($subNamespace)) {
            $namespace = "{$this->requestNamespace}\\{$subNamespace}";
        }

        // validator stub
        $validatorStub = $this->files->get("{$this->stubPath}Validator.stub");
        $dummyKey = [
            'dummyNamespace',
            'dummyResponsePath',
            'DummyClass',
            'dummyRules',
            'dummyErrorMessages',
            'dummyFailedValidation'
        ];
        $dummyValue = [
            $namespace,
            $failResponseClassPath,
            $className,
            $rules,
            $errorMessage,
            $failResponseStub
        ];
        $validatorStub = str_replace($dummyKey, $dummyValue, $validatorStub);
        $requestValidatorPath = "{$this->requestDirPath}/{$className}.php";
        if (!empty($subNamespace)) {
            $requestValidatorPath = "{$this->requestDirPath}/{$subNamespace}/{$className}.php";
        }
        if (!$this->files->isDirectory(dirname($requestValidatorPath))) {
            $this->files->makeDirectory(dirname($requestValidatorPath), 0777, true, true);
        }
        $this->files->put($requestValidatorPath, $validatorStub);
    }
}
